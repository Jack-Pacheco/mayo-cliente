export class Service {
    id?: number;
    name: string;
    image: string;
    status: number;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.name = '';
        this.image = '';
        this.status = null;
        this.created_at = '';
        this.updated_at = '';
    }
}

export class CreateService {
    id?: number;
    name: string;
    status: number;
}

export class EditService {
    id?: number;
    name: string;
    status: number;
}

export class ServiceMeasurement {
    id?: number;
    price: number;
    contractor_level_id: number;
    service_id: number;
    measurement_unit_id: number;
    created_at: string;
    updated_at: string;
}

export class CreateServiceMeasurement {
    id?: number;
    price: number;
    contractor_level_id: number;
    service_id: number;
    measurement_unit_id: number;
}

export class EditServiceMeasurement {
    id?: number;
    price: number;
    contractor_level_id: number;
    service_id: number;
    measurement_unit_id: number;
}