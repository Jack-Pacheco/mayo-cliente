export class User {
    id?: number;
    username: string;
    email: string;
    rating: number;
    crm_id: number;
    crm: any;
    services: any[];

    constructor() {
        this.id = null;
        this.username = '';
        this.email = '';
        this.rating = null;
        this.crm_id = null;
        this.crm = {};
        this.services = [];
    }
}

export class CreateUser {
    id?: number;
    username: string;
    email: string;
    password: string;
    password_confirmation: string;
    type_id: number;
    crm: {
        first_name: string;
        last_name: string;
        identification: string;
        gender: string;
    };
}

export class EditUser {
    id?: number;
    username: string;
    email: string;
    password: string;
    password_confirmation: string;
    type_id: number;
    crm: {
        first_name: string;
        last_name: string;
        identification: string;
        gender: string;
    };
    services: any[];
}

export class UserRating {
    id?: number;
    order_id: number;
    user_id: number;
    rater_id: number;
    comment: string;
    rating: number;
    created_at: string;
    updated_at: string;
}

export class CreateUserRating {
    id?: number;
    comment: string;
    rating: number;
    user_id: number;
    rater_id: number;
    order_id: number;
}

export class EditUserRating {
    id?: number;
    comment: string;
    rating: number;
    user_id: number;
    rater_id: number;
    order_id: number;
}