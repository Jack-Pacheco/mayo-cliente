export class Address {
    id?: number;
    address: string;
    name: string;
    building: string;
    number: string;
    street: string;
    sector: string;
    province: string;
    longitude: string;
    latitude: string;
    crm_id: number;
    address_type_id: number;
    address_type: string;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.address = '';
        this.name = '';
        this.building = '';
        this.number = '';
        this.street = '';
        this.sector = '';
        this.province = '';
        this.longitude = '';
        this.latitude = '';
        this.crm_id = null;
        this.address_type_id = null;
        this.address_type = '';
        this.created_at = '';
        this.updated_at = '';
    }
}

export class AddAddress {
    id?: number;
    address: string;
    name: string;
    building: string;
    number: string;
    street: string;
    sector: string;
    province: string;
    crm_id: number;
    address_type_id: number;
    longitude: string;
    latitude: string;
}

export class EditAddress {
    id?: number;
    address: string;
    name: string;
    building: string;
    number: string;
    street: string;
    sector: string;
    province: string;
    crm_id: number;
    address_type_id: number;
    longitude: string;
    latitude: string;
}