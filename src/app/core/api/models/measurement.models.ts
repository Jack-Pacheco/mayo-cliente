export class Measurement {
    id?: number;
    name: string;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.name = '';
        this.created_at = '';
        this.updated_at = '';
    }
}

export class CreateMeasurement {
    id?: number;
    name: string;
}

export class EditMeasurement {
    id?: number;
    name: string;
}