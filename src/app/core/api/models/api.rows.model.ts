import {IApiResponse} from './response.model';

export interface IApiRow<T> {
  total: number;
  rows: Array<T>;
}

export interface IApiRows<T> extends IApiResponse<IApiRow<T>> {
  code: number;
  result?: IApiRow<T>;
  response: string | null;
}
