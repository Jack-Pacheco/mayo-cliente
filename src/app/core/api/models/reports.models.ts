export class Report {
    id?: number;
    entity: string;
    total: number;
    name: string;
    email: string;
}

export class ServicesRecurrenceReport {
    id?: number;
    entity: string;
    total: number;
    name: string;
    email: string;
    service: string;
    service_id: number;
}