export class Notification {
    id?: number;
    title: string;
    message: string;
    read: number;
    entity_id: number;
    extra_content: string;
    user_id: number;
    notification_type_id: number;
    notification_type: string;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.title = '';
        this.message = '';
        this.read = null;
        this.entity_id = null;
        this.extra_content = '';
        this.user_id = null;
        this.notification_type_id = null;
        this.notification_type = '';
        this.created_at = '';
        this.updated_at = '';
    }
}