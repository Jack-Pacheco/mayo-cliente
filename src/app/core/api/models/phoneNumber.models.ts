export class PhoneNumber {
    id?: number;
    number: string;
    ext: string;
    name: string;
    phone_number_type_id: number;
    phone_number_type: string;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.number = '';
        this.ext = '';
        this.name = '';
        this.phone_number_type_id = null;
        this.phone_number_type = '';
        this.created_at = '';
        this.updated_at = '';
    }
}

export class AddPhoneNumber {
    id?: number;
    name: string;
    number: string;
    ext: string;
    phone_number_type_id: number;
    crm_id: number;
}

export class EditPhoneNumber {
    id?: number;
    name: string;
    number: string;
    ext: string;
    phone_number_type_id: number;
    crm_id: number;
}