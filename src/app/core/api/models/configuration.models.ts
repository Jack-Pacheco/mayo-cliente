export class Configuration {
    id?: number;
    name: string;
    value: string;
    created_at: string;
    updated_at: string;
}

export class CreateConfiguration {
    id?: number;
    name: string;
    value: string;
}

export class EditConfiguration {
    id?: number;
    name: string;
    value: string;
}