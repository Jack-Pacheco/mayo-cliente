export class CreditCard {
    id?: number;
    number: string;
    name: string;
    month: number;
    year: number;
    cvc: string
    fiscal_proof_number: string;
    user_id: number;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.number = '';
        this.name = '';
        this.month = null;
        this.year = null;
        this.cvc = '';
        this.fiscal_proof_number = '';
        this.user_id = null;
        this.created_at = '';
        this.updated_at = '';
    }
}

export class AddCreditCard {
    id?: number;
    number: string;
    name: string;
    month: number;
    year: number;
    cvc: string;
    fiscal_proof_number: string;
    user_id: number;
}

export class EditCreditCard {
    id?: number;
    number: string;
    name: string;
    month: number;
    year: number;
    cvc: string;
    fiscal_proof_number: string;
    user_id: number;
}