export class Metrics {
    order_count: number;
    orders_total: number;
    orders_total_average: number;
    orders_execution_average: number;
    services_ordered: any[];
    current_orders: any[];
}