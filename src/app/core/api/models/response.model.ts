export interface IApiResponse<T> {
    code: number;
    result?: T;
    response: string | null;
  }
