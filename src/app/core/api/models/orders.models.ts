export class Orders {
    id?: number;
    order_number: string;
    service_amount: number;
    material_amount: number;
    itbis: number;
    total_amount: number;
    unit_price: number;
    unit_qty: number;
    programmed: number;
    programmed_date: string;
    acceptance_date: string;
    completion_date: string;
    mayo_bought_materials: number;
    receive_by: string;
    status_id: number;
    state_id: number;
    service_id: number;
    user_client_id: number;
    user_contractor_id: number;
    measurement_unit_id: number;
    contractor_level_id: number;
    address_id: number;
    created_at: string;
    updated_at: string;

    constructor() {
        this.id = null;
        this.order_number = '';
        this.service_amount = null;
        this.material_amount = null;
        this.itbis = null;
        this.total_amount = null;
        this.unit_price = null;
        this.unit_qty = null;
        this.programmed = null;
        this.programmed_date = '';
        this.acceptance_date = '';
        this.completion_date = '';
        this.mayo_bought_materials = null;
        this.receive_by = '';
        this.status_id = null;
        this.state_id = null;
        this.service_id = null;
        this.user_client_id = null;
        this.user_contractor_id = null;
        this.measurement_unit_id = null;
        this.contractor_level_id = null;
        this.address_id = null;
        this.created_at = '';
        this.updated_at = '';
    }
}

export class CreateOrder {
    mayo_bought_materials: number;
    receive_by: string;
    service_id: number;
    user_client_id: number;
    address_id: number;
    state_id: number;
    unit_qty: number;
    measurement_unit_id: number;
    contractor_level_id: number;
    programmed: number;
    programmed_date: string;
}

export class EditOrder {
    mayo_bought_materials: number;
    receive_by: string;
    service_id: number;
    user_client_id: number;
    address_id: number;
    state_id: number;
    unit_qty: number;
    measurement_unit_id: number;
    contractor_level_id: number;
    programmed: number;
    programmed_date: string;
}

export class OrderClaim {
    id?: number;
    description: string;
    order_id: number;
    claim_state_id: number;
    claim_reason_id: number;
    created_at: string;
    updated_at: string;
}

export class CreateOrderClaim {
    id?: number;
    description: string;
    claim_state_id: number;
    order_id: number;
    claim_reason_id: number;
}

export class EditOrderClaim {
    id?: number;
    description: string;
    claim_state_id: number;
    order_id: number;
    claim_reason_id: number;
}

export class OrderRequestMaterial {
    id?: number;
    total: number;
    image: string;
    description: string;
    acepted: number;
    order_id: number;
    created_at: string;
    updated_at: string;
}

export class CreateOrderRequestMaterial {
    id?: number;
    total: number;
    description: string;
    order_id: number;
    acepted: number;
}

export class EditOrderRequestMaterial {
    id?: number;
    total: number;
    description: string;
    order_id: number;
    acepted: number;
}

export class OrderMessage {
    id?: number;
    order_id: number;
    message: string;
    message_read: number;
    sender_id: number;
    created_at: string;
    updated_at: string;
}

export class CreateOrderMessage {
    id?: number;
    order_id: number;
    sender_id: number;
    message: string;
    message_read: number;
}

export class EditOrderMessage {
    id?: number;
    order_id: number;
    sender_id: number;
    message: string;
    message_read: number;
}

export class OrderCancelation {
    id?: number;
    order_id: number;
    description: string;
    created_at: string;
    updated_at: string;
}

export class CreateOrderCancelation {
    id?: number;
    description: string;
    order_id: number;
}

export class EditOrderCancelation {
    id?: number;
    description: string;
    order_id: number;
}