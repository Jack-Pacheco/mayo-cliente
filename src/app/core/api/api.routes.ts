import { environment } from '../../../environments/environment';
import '../../lib/globals/string.extensions';

export const apiRoutes = {
    auth: {
        login: '/login',
        register: '/register'
    },
    user: {
        index: '/user',
        rating: '/user/rating'
    },
    services: {
        index: '/service',
        measurement: '/service/measurement'
    },
    phoneNumber: {
        index: '/phone_number'
    },
    notification: {
        index: '/notification'
    },
    address: {
        index: '/address'
    },
    creditCard: {
        index: '/credit_card'
    },
    orders: {
        index: '/order',
        claim: '/order/claim',
        requestMaterial: '/order/request_material',
        message: '/order/message',
        cancelation: '/order/cancelation'
    },
    measurement: {
        index: '/measurement_unit'
    },
    configuration: {
        index: '/configuration'
    },
    metrics: {
        index: '/metric'
    },
    reports: {
        orders: {
            total: '/report/orders/total',
            duration: '/report/orders/duration',
            claim: '/report/orders/claim'
        },
        users: {
            rating: '/report/users/rating'
        },
        services: {
            recurrence: '/report/services/recurrence'
        },
        materials: {
            average: '/report/materials/average'
        }
    }
}

export function getRoute(path: string): string {
    return environment.api_url + path;
}
