import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import { Configuration, CreateConfiguration, EditConfiguration } from '../core/api/models/configuration.models';
@Injectable()
export class ConfigurationService {
  private route: string;
  constructor(private http: HttpClient) { 
    this.route = getRoute(apiRoutes.configuration.index);
  }

  getConfigurationVariables(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route);
  }
  getConfigurationVariableById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
  postConfigurationVariable(configurationVariable: CreateConfiguration): Observable<any> {
    const route = this.route;
    return this.http.post<IApiResponse<any>>(route, configurationVariable);
  }
  putConfigurationVariable(id: number, configurationVariable: EditConfiguration): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, configurationVariable);
  }
  deleteConfigurationVariable(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }
}
