import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import { CreditCard, AddCreditCard, EditCreditCard } from '../core/api/models/creditCard.models';
@Injectable()
export class CreditCardService {
  private route: string;
  constructor(private http: HttpClient) {
    this.route = getRoute(apiRoutes.creditCard.index);
   }

   getCreditCards(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route);
   }
   getCreditCardById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postCreditCard(creditCard: AddCreditCard): Observable<any> {
     const route = this.route;
     return this.http.post<IApiResponse<any>>(route, creditCard);
   }
   putCreditCard(id: number, creditCard: EditCreditCard): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, creditCard);
   }
   deleteCreditCard(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }
}
