import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import { Address, AddAddress, EditAddress } from '../core/api/models/addresses.models';
@Injectable()
export class AddressesService {
  private route: string;
  constructor(private http: HttpClient) { 
    this.route = getRoute(apiRoutes.address.index);
  }

  getAddresses(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route);
  }
  getAddressById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
  postAddress(address: AddAddress): Observable<any> {
    const route = this.route;
    return this.http.post<IApiResponse<any>>(route, address);
  }
  putAddress(id: number, address: EditAddress): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, address);
  }
  deleteAddress(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }
}
