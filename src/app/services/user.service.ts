import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import { User, CreateUser, EditUser, UserRating, CreateUserRating, EditUserRating } from '../core/api/models/user.models';

@Injectable()
export class UserService {
  private route: string;
  private ratingRoute: string;
  constructor(private http: HttpClient) {
    this.route = getRoute(apiRoutes.user.index);
    this.ratingRoute = getRoute(apiRoutes.user.rating);
   }

  getUsers(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route); 
  }
  getUserById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
  postUser(user: CreateUser): Observable<any> {
    const route = this.route;
    return this.http.post<IApiResponse<any>>(route, user);
  }
  putUser(id: number, user: EditUser): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, user);
  }
  deleteUser(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }
  postUserImage(id: number, image: any): Observable<any> {
    const route = `${this.route}/${id}/image`;
    return this.http.post<IApiResponse<any>>(route, image);
  }

  getUsersRating(): Observable<any> {
    const route = this.ratingRoute;
    return this.http.get<IApiResponse<any>>(route);
  }
  getUserRatingById(id: number): Observable<any> {
    const route = `${this.ratingRoute}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
  postUserRating(userRating: CreateUserRating): Observable<any> {
    const route = this.ratingRoute;
    return this.http.post<IApiResponse<any>>(route, userRating);
  }
  putUserRating(id: number, userRating: EditUserRating): Observable<any> {
    const route = `${this.ratingRoute}/${id}`;
    return this.http.put<IApiResponse<any>>(route, userRating);
  }
  deleteUserRating(id: number): Observable<any> {
    const route = `${this.ratingRoute}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }

}
