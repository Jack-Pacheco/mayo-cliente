import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import {Notification} from '../core/api/models/notifications.models';
@Injectable()
export class NotificationsService {
  private route: string;
  constructor(private http: HttpClient) { 
    this.route = getRoute(apiRoutes.notification.index);
  }

  getNotifications(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route);
  }
  getNotificationById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
  deleteNotification(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }
}
