import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import {
  Orders, CreateOrder, EditOrder,
  OrderClaim, CreateOrderClaim, EditOrderClaim,
  OrderRequestMaterial, CreateOrderRequestMaterial, EditOrderRequestMaterial,
  OrderMessage, CreateOrderMessage, EditOrderMessage,
  OrderCancelation, CreateOrderCancelation, EditOrderCancelation
} from '../core/api/models/orders.models';
@Injectable()
export class OrderService {
  private route: string;
  private claimRoute: string;
  private requestMaterialRoute: string;
  private messageRoute: string;
  private cancelationRoute: string;
  constructor(private http: HttpClient) {
    this.route = getRoute(apiRoutes.orders.index);
    this.claimRoute = getRoute(apiRoutes.orders.claim);
    this.requestMaterialRoute = getRoute(apiRoutes.orders.requestMaterial);
    this.messageRoute = getRoute(apiRoutes.orders.message);
    this.cancelationRoute = getRoute(apiRoutes.orders.cancelation);
   }

   getOrders(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route);
   }
   getOrderById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postOrder(order: CreateOrder): Observable<any> {
    const route = this.route;
    return this.http.post<IApiResponse<any>>(route, order);
   }
   putOrder(id: number, order: EditOrder): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, order);
   }
   deleteOrder(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }

   getOrderClaims(): Observable<any> {
    const route = this.claimRoute;
    return this.http.get<IApiResponse<any>>(route);
   }
   getOrderClaimById(id: number): Observable<any> {
    const route = `${this.claimRoute}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postOrderClaim(orderClaim: CreateOrderClaim): Observable<any> {
    const route = this.claimRoute;
    return this.http.post<IApiResponse<any>>(route, orderClaim);
   }
   putOrderClaim(id: number, orderClaim: EditOrderClaim): Observable<any> {
     const route = `${this.claimRoute}/${id}`;
     return this.http.put<IApiResponse<any>>(route, orderClaim);
   }
   deleteOrderClaim(id: number): Observable<any> {
    const route = `${this.claimRoute}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }

   getOrderRequestsMaterial(): Observable<any> {
    const route = this.requestMaterialRoute;
    return this.http.get<IApiResponse<any>>(route);
   }
   getOrderRequestMaterialById(id: number): Observable<any> {
    const route = `${this.requestMaterialRoute}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postOrderRequestMaterial(ordeRequestMaterial: CreateOrderRequestMaterial): Observable<any> {
    const route = this.requestMaterialRoute;
    return this.http.post<IApiResponse<any>>(route, ordeRequestMaterial);
   }
   putOrderRequestMaterial(id: number, ordeRequestMaterial: EditOrderRequestMaterial): Observable<any> {
    const route = `${this.requestMaterialRoute}/${id}`;
    return this.http.put<IApiResponse<any>>(route, ordeRequestMaterial);
   }
   deleteOrderRequestMaterial(id: number): Observable<any> {
    const route = `${this.requestMaterialRoute}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }
   postOrderRequestMaterialImage(id: number, image: any): Observable<any> {
    const route = `${this.requestMaterialRoute}/${id}/image`;
    return this.http.post<IApiResponse<any>>(route, image);
   }

   getOrderMessages(): Observable<any> {
    const route = this.messageRoute;
    return this.http.get<IApiResponse<any>>(route);
   }
   getOrderMessageById(id: number): Observable<any> {
    const route = `${this.messageRoute}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postOrderMessage(orderMessage: CreateOrderMessage): Observable<any> {
    const route = this.messageRoute;
    return this.http.post<IApiResponse<any>>(route, orderMessage);
   }
   putOrderMessage(id: number, orderMessage: EditOrderMessage): Observable<any> {
    const route = `${this.messageRoute}/${id}`;
    return this.http.put<IApiResponse<any>>(route, orderMessage);
   }
   deleteOrderMessage(id: number): Observable<any> {
    const route = `${this.messageRoute}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }

   getOrdersCancelations(): Observable<any> {
    const route = this.cancelationRoute;
    return this.http.get<IApiResponse<any>>(route);
   }
   getOrderCancelationById(id: number): Observable<any> {
    const route = `${this.cancelationRoute}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postOrderCancelation(orderCancelation: CreateOrderCancelation): Observable<any> {
    const route = this.cancelationRoute;
    return this.http.post<IApiResponse<any>>(route, orderCancelation);
   }
   putOrderCancelation(id: number, orderCancelation: EditOrderCancelation): Observable<any> {
    const route = `${this.cancelationRoute}/${id}`;
    return this.http.put<IApiResponse<any>>(route, orderCancelation);
   }
   deleteOrderCancelation(id: number): Observable<any> {
    const route = `${this.cancelationRoute}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }
}
