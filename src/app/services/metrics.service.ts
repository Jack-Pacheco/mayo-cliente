import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model'; 
import { Metrics } from '../core/api/models/metrics.model';
@Injectable()
export class MetricsService {
  private route: string;
  constructor(private http: HttpClient) { 
    this.route = getRoute(apiRoutes.metrics.index);
  }
  getMetric(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
}
