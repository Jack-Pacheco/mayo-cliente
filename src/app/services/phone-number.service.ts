import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import { PhoneNumber, AddPhoneNumber, EditPhoneNumber } from '../core/api/models/phoneNumber.models';
@Injectable()
export class PhoneNumberService {
  private route: string;
  constructor(private http: HttpClient) {
    this.route = getRoute(apiRoutes.phoneNumber.index);
   }

   getPhoneNumbers(): Observable<any> {
    const route = this.route;
    return this.http.get<IApiResponse<any>>(route);
   }
   getPhoneNumberById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postPhoneNumber(id: number, phoneNumber: AddPhoneNumber): Observable<any> {
     const route = `${this.route}/${id}`;
     return this.http.post<IApiResponse<any>>(route, phoneNumber);
   }
   putPhoneNumber(id: number, phoneNumber: EditPhoneNumber): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, phoneNumber);
   }
   deletePhoneNumber(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
   }
}
