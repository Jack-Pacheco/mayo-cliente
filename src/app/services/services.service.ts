import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { apiRoutes, getRoute } from '../core';
import { IApiResponse } from '../core/api/models/response.model';
import { Service, CreateService, EditService, ServiceMeasurement, CreateServiceMeasurement, EditServiceMeasurement } from '../core/api/models/services.models';
@Injectable()
export class ServicesService {
  private route: string;
  private measurementRoute: string;
  constructor(private http: HttpClient) {
    this.route = getRoute(apiRoutes.services.index);
    this.measurementRoute = getRoute(apiRoutes.services.measurement);
   }
   
   getServices(): Observable<any> {
     const route = this.route;
     return this.http.get<IApiResponse<any>>(route);
   }
   getServiceById(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   getSuggestedServices(id: number): Observable<any> {
    const route = `${this.route}/suggested/${id}`;
    return this.http.get<IApiResponse<any>>(route);
   }
   postService(service: CreateService): Observable<any> {
    const route = this.route;
    return this.http.post<IApiResponse<any>>(route, service);
   }
   putService(id: number, service: EditService): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.put<IApiResponse<any>>(route, service);
  }
  deleteService(id: number): Observable<any> {
    const route = `${this.route}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }
  postServiceImage(id: number, image: any): Observable<any> {
    const route = `${this.route}/${id}/image`;
    return this.http.post<IApiResponse<any>>(route, image);
  }

  getServiceMeasurements(): Observable<any> {
    const route = this.measurementRoute;
     return this.http.get<IApiResponse<any>>(route);
  }
  getServiceMeasurementById(id: number): Observable<any> {
    const route = `${this.measurementRoute}/${id}`;
    return this.http.get<IApiResponse<any>>(route);
  }
  postServiceMeasurement(serviceMeasurement: CreateServiceMeasurement): Observable<any> {
    const route = this.measurementRoute;
    return this.http.post<IApiResponse<any>>(route, serviceMeasurement);
  }
  putServiceMeasurement(id: number, serviceMeasurement: EditServiceMeasurement): Observable<any> {
    const route = `${this.measurementRoute}/${id}`;
    return this.http.put<IApiResponse<any>>(route, serviceMeasurement);
  }
  deleteServiceMeasurement(id: number): Observable<any> {
    const route = `${this.measurementRoute}/${id}`;
    return this.http.delete<IApiResponse<any>>(route);
  }
}
