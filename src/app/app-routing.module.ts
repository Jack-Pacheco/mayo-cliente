import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth/auth-guard.service';

const routes: Routes = [
{ 
  path: '',
  redirectTo: 'home',
  pathMatch: 'full'
},
{ 
  path: 'home',
  canActivate: [AuthGuardService],
  loadChildren: './home/home.module#HomePageModule'
},
{ 
  path: 'login',
  loadChildren: './pages/auth/login/login.module#LoginPageModule'
},
{ 
  path: 'register',
  loadChildren: './pages/auth/register/register.module#RegisterPageModule'
},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
