export {};

declare global {
    interface String {
        format(...params: string[]): string;
    }
}

String.prototype.format = function() {
    const args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
    return typeof args[number] !== 'undefined'
        ? args[number]
        : match;
    });
};
